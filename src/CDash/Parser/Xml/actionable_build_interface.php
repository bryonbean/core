<?php
namespace CDash\Parser\Xml;
/**
 * ActionableHandler
 */
interface ActionableBuildInterface
{
    /**
     * @return Build[]
     */
    public function getActionableBuilds();
}
